# About

The project aims to create a whatsapp message gateway using low costs Android devices and the calabash framework.

https://developer.xamarin.com/guides/testcloud/calabash/working-with/testing-on-devices/

# PoC

* Calabash .: [https://github.com/calabash/calabash-android](https://github.com/calabash/calabash-android)

* Amazon Device Farm .: [https://aws.amazon.com/pt/device-farm](https://aws.amazon.com/pt/device-farm/)

* Apium .: [http://appium.io/](http://appium.io/)

* WhatsApp APK .: [https://www.whatsapp.com/android/](https://www.whatsapp.com/android/) or [WhatsApp.apk](https://www.cdn.whatsapp.net/android/2.16.339/WhatsApp.apk)

# CALABASH

## Creating the local environment

Download the Android SDK and simply setup the following env variables:

```
export ANDROID_HOME=$HOME/Android/Sdk
export PATH="$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"
```

## Testing the WhatsApp apk

```
calabash-android build resources/WhatsApp.apk

calabash-android resign resources/WhatsApp.apk

calabash-android console resources/WhatsApp.apk
```

## Calabash console

Since you've executed the previous steps, you will fall in the IRB console. So, before you proceed, you must to start the AVD emulator like that:

```
emulator -avd Nexus_4_API_25
```

Where **Nexus_4_API_25** is the virtual machine that you created (see at: $HOME/.android/avd).

Now, you can install the Whatsapp.apk:

```
irb(main):001:0> reinstall_apps
```

Don't worry about messages like that: 

```
java.lang.IllegalArgumentException: Unknown package: com.whatsapp.test
```

And now, you must run the test server and you'll see the Whatsapp application openning in the emulator.

```
irb(main):002:0> start_test_server_in_background
```

# APPIUM

## Installation

The install process is better when is made by installing the ultimate node version through the linuxbrew:

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"
PATH="$HOME/.linuxbrew/bin:$PATH"

brew install node
brew link node
npm -g install appium
```

## Creating the local environment

Create a appium.txt with the folliwng content:
```
[caps]
platformName = "android"
app = "./resources/WhatsApp.apk"
```

You'll need a Gemfile with the following content:

```
source 'https://rubygems.org'

gem 'appium_lib'

group :development do
  gem 'appium_console'
  gem 'pry'
end

```

## Inspecting the WhatsApp application

The inspection is made with the **uiautomatorviewer**, a tool from Android SDK.

## Using a real Android device

Go to the Settings -> Build number -> Tap 7 times

Enable the Developer Mode and USB Debugging on the new entry at the Settings Menu

In the shell, type this:
```
adb devices
```

Your device will ask for permissions (then you'll need to grant it).

After it, you need to update the abd devices:

```
android update adb
adb kill-server
adb start-server
```

## Testing the Appium

In one shell, start the appium server:

```
appium
```

And after it, start the appium console (arc) - you need to have a appium.txt file on the current directoty:

```
arc
```

Done! Now you are in the Appium Console (over ruby pry).

