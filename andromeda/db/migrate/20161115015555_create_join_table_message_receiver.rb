class CreateJoinTableMessageReceiver < ActiveRecord::Migration[5.0]
  def change
    create_join_table :Messages, :Receivers do |t|
      # t.index [:message_id, :receiver_id]
      # t.index [:receiver_id, :message_id]
    end
  end
end
