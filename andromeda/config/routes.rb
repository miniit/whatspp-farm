Rails.application.routes.draw do
  devise_for :users
  resources :messages
  resources :receivers

  get 'welcome/index'

  resources :messages
  authenticated :user do
    root to: 'messages#index', as: 'authenticated_root'
  end

  root to: 'welcome#index' 

end
