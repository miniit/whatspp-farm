# WhatsApp Appium Library
# @author Ricardo Ichizo
class Whatsapp

  attr_accessor :appium, :selenium

  # @param reset [Boolean, false] reset the app configuration
  def initialize(reset=false)
    # capabilities: https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/caps.md
    capabilities = {
      'platformName' => 'Android',
      'deviceName' => 'Nexus_S5',
      'newCommandTimeout' => 1800,
      'noReset' => !reset,
      'appPackage' => 'com.whatsapp',
      'appActivity' => 'com.whatsapp.Main'
    }
    @appium = Appium::Driver.new(caps: capabilities)
    @selenium = @appium.start_driver
  end

  # Locates a contact in the WhatsApp contact list
  # @param contact_identifier [String] the contact_identifier
  # @return element [Selenium::WebDriver::Element]
  def locate_contact(contact_identifier)
    # click on the "Contacts" tab
    elem = @selenium.find_element(:xpath,
    '//android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.TextView[@resource-id="com.whatsapp:id/tab"]')
    elem.click

    elem = @appium.scroll_to(contact_identifier)
    if elem.text == contact_identifier
      elem
    else
      nil
    end
  end

  # Adds a new contact to the contact list
  # @param phone [String] phone number
  # @param name [String] contact name
  def add_new_contact(phone, name)
    if phone != nil and name != nil
      # new contact
      elem = @selenium.find_element(:id, 'com.whatsapp:id/menuitem_new_contact')
      elem.click
      # local contact (it will use the phone, not the Google account)
      elem = @selenium.find_element(:xpath,
      '//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView')
      elem.click
      # remove the virtual keyboard
      @selenium.navigate.back
      # text inputs
      inputs = @selenium.find_elements(:class, 'android.widget.EditText')
      inputs[0].send_keys(name)
      inputs[1].send_keys(phone)
      # remove the virtual keyboard
      @selenium.navigate.back
      # conclude
      @selenium.find_element(:id, 'com.android.contacts:id/icon').click
      @selenium.navigate.back
    end
  end

  #
  def remove_contact(phone, name=nil)
  end

  # Send a message to a contact
  # @param contact [Selenium::WebDriver::Element] contact
  # @param msg [String] message to send
  def send_message(contact, msg)
    if contact == nil
      return
    end

    contact.click

    text_field = @selenium.find_element :id, 'com.whatsapp:id/entry'
    text_field.send_keys msg

    send_btn = @selenium.find_element :id, 'com.whatsapp:id/send'
    send_btn.click

    @selenium.navigate.back
    @selenium.navigate.back
  end

end
