json.extract! message, :id, :subject, :message, :created_at, :updated_at
json.url message_url(message, format: :json)